#!/bin/bash

if [ -z "$1" ]
then
	test_dir="test"
else
	test_dir="$1"
fi

if [ ! -d "$test_dir" ]
then
	>&2 echo "'$test_dir' isn't a valid directory"
	exit 1
fi

for t in $(find "$test_dir" -name '*.sh') ; do
	. "$t"
done

test_cases=$(declare -F | sed 's/declare -f //' | grep '^test_')

total=0
failures=0
failed_test_cases=()
for tc in $test_cases ; do
	echo "Executing ${tc}..."
	$tc
	if [ $? -ne 0 ] ; then
		failures=$(expr ${failures} + 1)
		failed_test_cases+=(${tc})
	fi
	total=$(expr ${total} + 1)
done

echo "Test suite completed: total test cases ran: ${total}. Failures: ${failures}"

if [ $failures -ne 0 ] ; then
	echo "Failed test cases:"
	for ftc in "${failed_test_cases[@]}" ; do
		echo " ${ftc}"
	done
	exit 1
fi
exit 0
