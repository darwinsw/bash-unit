# bash-unit
A simple tool for testing bash scripts

## Installation
Open a terminal and execute following commands:
```bash
# Clone bash-unit's Git repository
git clone https://gitlab.com/darwinsw/bash-unit.git
# Enter the local clone
cd bash-unit
# Add bash-unit's directory to the `$PATH`
cat <<EOF >> ~/.bashrc 
export PATH="\$PATH:$(pwd)"
alias bash-unit='bash-unit.sh'
EOF
```
Then you can open a new terminal and launch `bash-unit`: you should get an output similar to
```
'test' isn't a valid directory
```
Launching
```bash
 bash-unit $(dirname $(which bash-unit.sh))/examples/0/test
```
you should get the following output instead:
```
Executing test_mocking_stdin...
Executing test_read_input_from_stdin...
Executing test_spying_stderr...
Executing test_spying_stdout...
Executing test_use_first_cli_parameter...
Test suite completed: total test cases ran: 5. Failures: 0
```
## Defining test cases
A `bash-unit` *test case* is `Bash` function whose name starts with `test_`.
Its exit code is evaluated as success (when 0) or failure (when not 0).
**Examples**:
```bash
test_a () {
  return 0 # should pass
}

test_b () {
  test 1 -ge 1 # should pass
}

test_c () {
  test -d /var/tmp # should pass
}

test_d () {
  test -f /etc # should fail
}
```
## Spying *stdout*
You can collect *standard output* through *command expansion* and evaluate your assertions against it:
```bash
# Should pass
test_spying_stdout () {
	output=$(echo -e "1 2 3 4 5 6")
	echo $output | grep "1 2 3 4 5 6" > /dev/null
}
```
## Mocking *stdin*
You can provide *fake*, hardcoded content to *standard input*, in order to avoid interactivity during test cases execution:
```bash
# Should pass: grep exits with 0 when pattern is found
test_mocking_stdin_passes () {
	cat <<EOF | grep "AAA" > /dev/null
XXX
YYY
AAA
ZZZ
EOF
}

# Should fail: grep exits with not 0 when pattern is not found
test_mocking_stdin_fails () {
	cat <<EOF | grep "DDD" > /dev/null
XXX
YYY
AAA
ZZZ
EOF
}
```
## Spying *stderr*
You can *spy* *stderr* in a similar way to how you can *spy* *stdin*, providing additional redirections:
```bash
# Should pass
test_spying_stderr () {
	error=$(cat /proc/crypto no-file.txt  2>&1 > /dev/null)
	echo $error | grep "no-file.txt: No such file or directory" > /dev/null
}
```