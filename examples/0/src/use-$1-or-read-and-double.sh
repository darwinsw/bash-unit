#!/bin/bash

if [ $# -gt 1 ]
then
	echo "Only one parameter supported"
	exit 1
fi
if [ $# -eq 1 ]
then
	param=$1
else
	echo "Please insert a numeric value"
	read param
fi
expr 2 '*' $param