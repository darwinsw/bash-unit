#!/bin/bash

test_spying_stdout () {
	output=$(echo -e "1 2 3 4 5 6")
	echo $output | grep "1 2 3 4 5 6" > /dev/null
}

test_spying_stderr () {
	output=$(cat /proc/crypto no-file.txt  2>&1 > /dev/null)
	echo $output | grep "no-file.txt: No such file or directory" > /dev/null
}