#!/bin/bash

current_dir=$(dirname $BASH_SOURCE[0])

test_use_first_cli_parameter () {
	output=$("$current_dir/../src/use-\$1-or-read-and-double.sh" 11)
	test "$output" -eq 22
}

test_read_input_from_stdin () {
	output=$(cat <<EOF | $current_dir/../src/use-\$1-or-read-and-double.sh
19
EOF
)
	echo $output | grep "38$" > /dev/null
}
